module gitlab.com/bersace/pointeuse

go 1.21

require (
	github.com/goodsign/monday v1.0.0
	github.com/joho/godotenv v1.4.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lmittmann/tint v1.0.4
	github.com/matrix-org/gomatrix v0.0.0-20210324163249-be2af5ef2e16
	golang.org/x/exp v0.0.0-20230321023759-10a507213a29
)
