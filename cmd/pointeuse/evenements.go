package main

import (
	"fmt"
	"time"

	"log/slog"

	"github.com/matrix-org/gomatrix"
)

func RécupérerDernierÉvènement(c *gomatrix.Client, roomid string) (from string, last gomatrix.Event, err error) {
	resp, err := c.SyncRequest(5, "", "", false, "offline")
	if err != nil {
		return
	}

	from = resp.NextBatch
	events := resp.Rooms.Join[roomid].Timeline.Events
	last = events[len(events)-1]
	return
}

func RécupérerIdSalon(c *gomatrix.Client, alias string) (string, error) {
	body := map[string]interface{}{
		"filter": map[string]interface{}{
			"generic_search_term": alias,
		},
	}
	url := c.BuildURL("publicRooms")
	var resp gomatrix.RespPublicRooms
	err := c.MakeRequest("POST", url, body, &resp)
	if err != nil {
		return "", err
	}
	return resp.Chunk[0].RoomID, nil
}

type TypeÉvènement int64

const (
	Message TypeÉvènement = iota
	PremierMessage
	Retour
)

type Évènement struct {
	Date    time.Time
	Message string
	Type    TypeÉvènement
}

type ListeurÉvènements struct {
	ch   chan Évènement
	err  error
	item Évènement
}

func NouveauListeurÉvènements(client *gomatrix.Client, idsalon, idutilisateur, from string, absent bool, butoir time.Time) ListeurÉvènements {
	l := ListeurÉvènements{}
	l.ch = l.iterÉvènements(client, idsalon, idutilisateur, from, absent, butoir)
	return l
}

func (l *ListeurÉvènements) Next() bool {
	e, ok := <-l.ch
	if !ok {
		return false
	}
	if l.err != nil {
		return false
	}
	l.item = e
	return true
}

func (l ListeurÉvènements) Err() error {
	return l.err
}

func (l ListeurÉvènements) Évènement() Évènement {
	return l.item
}

func (l *ListeurÉvènements) iterÉvènements(client *gomatrix.Client, idsalon, idutilisateur, from string, absent bool, butoir time.Time) chan Évènement {
	ch := make(chan Évènement)
	go func() {
		defer close(ch)
		slog.Debug("Récupération l'historique.", "butoir", butoir)
		if !absent {
			mxe := ForgerMessageMaintenant(idutilisateur, idsalon)
			e, err := FabriquerÉvènement(mxe)
			if err != nil {
				l.err = err
				return
			}
			ch <- e
		}

		for {
			slog.Debug("Téléchargement de messages.", "salon", idsalon, "from", from)
			resp, err := client.Messages(idsalon, from, "", 'b', 1000)
			if err != nil {
				l.err = err
				return
			}

			for _, mxe := range resp.Chunk {
				if mxe.Sender != idutilisateur {
					continue
				}
				if _, ok := mxe.Content["m.new_content"]; ok {
					// Sauter les éditions. Elles désordonnent les messages.
					continue
				}

				e, err := FabriquerÉvènement(mxe)
				if err != nil {
					l.err = err
					return
				}
				if e.Date.Before(butoir) {
					return
				}
				ch <- e
			}

			if resp.End == "" {
				break
			} else {
				from = resp.End
			}
		}
		slog.Debug("Fin du téléchargement.")
	}()
	return ch
}

func ForgerMessageMaintenant(userid, roomid string) gomatrix.Event {
	// Forger un pseudo évènement de présence.
	return gomatrix.Event{
		Timestamp: time.Now().Unix() * 1000,
		Sender:    userid,
		RoomID:    roomid,
		Type:      "m.room.message",
		Content: map[string]interface{}{
			"body": "*** Exécution de la pointeuse. ***",
		},
	}
}

// Crée un évènement de la pointeuse à partir d'un évènement Matrix.
func FabriquerÉvènement(mxe gomatrix.Event) (ev Évènement, err error) {
	ev = Évènement{}

	sec := mxe.Timestamp / 1000
	nsec := 1000 * 1000 * (mxe.Timestamp % 1000)
	ev.Date = time.Unix(sec, nsec)

	switch mxe.Type {
	case "m.room.message":
		ev.Message = mxe.Content["body"].(string)
		if ev.Message == "re" {
			ev.Type = Retour
		} else {
			ev.Type = Message
		}
	case "m.reaction":
		ev.Type = Message
		reaction, ok := mxe.Content["m.relates_to"].(map[string]interface{})
		if ok {
			ev.Message = reaction["key"].(string)
		} else {
			ev.Message = "Rédaction de réaction."
		}
	default:
		err = fmt.Errorf("Type d'évènement inconnu: %s", mxe.Type)
	}
	return
}
