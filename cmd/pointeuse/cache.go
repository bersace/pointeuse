package main

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

type Cache struct {
	dossier string
}

func OuvrirCache() (Cache, error) {
	dossier, err := os.UserCacheDir()
	if err != nil {
		return Cache{}, fmt.Errorf("cache: %w", err)
	}
	c := Cache{dossier: filepath.Join(dossier, "pointeuse")}

	info, err := os.Stat(c.dossier)
	if os.IsNotExist(err) {
		err := os.MkdirAll(c.dossier, 0700)
		if err != nil {
			return c, err
		}
	} else if err != nil {
		return c, fmt.Errorf("stat: %w", err)
	} else if !info.IsDir() {
		return c, fmt.Errorf("dossier de cache non-dossier")
	}
	return c, nil
}

func (c Cache) Get(clef string) (interface{}, error) {
	chemin := filepath.Join(c.dossier, clef)
	info, err := os.Stat(chemin)
	if os.IsNotExist(err) {
		return nil, nil
	} else if err != nil {
		return nil, fmt.Errorf("cache: %w", err)
	} else if !info.Mode().IsRegular() {
		return nil, fmt.Errorf("entrée du cache irrégulier")
	}
	fo, err := os.Open(chemin)
	if err != nil {
		return nil, fmt.Errorf("cache: %w", err)
	}
	defer fo.Close()
	données, err := io.ReadAll(fo)
	if err != nil {
		return nil, fmt.Errorf("cache: %w", err)
	}
	var sortie interface{}
	err = json.Unmarshal([]byte(données), &sortie)
	if err != nil {
		return nil, fmt.Errorf("cache: json: %w", err)
	}
	return sortie, nil
}

func (c Cache) Set(clef string, value interface{}) error {
	chemin := filepath.Join(c.dossier, clef)
	fo, err := os.Create(chemin)
	if err != nil {
		return fmt.Errorf("cache: %w", err)
	}
	defer fo.Close()
	enc := json.NewEncoder(fo)
	err = enc.Encode(value)
	if err != nil {
		return fmt.Errorf("cache: json: %w", err)
	}
	return nil
}

type FauxCache struct{}

func (c FauxCache) Get(_ string) (interface{}, error) {
	return nil, nil
}

func (c FauxCache) Set(_ string, _ interface{}) error {
	return nil
}
