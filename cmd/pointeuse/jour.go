package main

import (
	"fmt"
	"time"

	"log/slog"
)

type Cacheur interface {
	Get(string) (interface{}, error)
	Set(string, interface{}) error
}

type Jour struct {
	Date       time.Time
	Évènements []Évènement
}

func (j *Jour) Ajoute(e Évènement) {
	if e.Date.IsZero() {
		return
	}
	j.Évènements = append([]Évènement{e}, j.Évènements...)
}

func (j Jour) Contient(d time.Time) bool {
	return d.Truncate(24*time.Hour) == j.Date
}

func (j *Jour) ChargeCache(cache Cacheur) (bool, error) {
	clef := j.Date.Format(YYYYMMDD)
	données, err := cache.Get(clef)
	if err != nil {
		return false, fmt.Errorf("cache: %w", err)
	}
	if nil == données {
		return false, nil
	}
	l, ok := données.([]interface{})
	if !ok {
		return false, fmt.Errorf("cache corrompu")
	}
	for _, i := range l {
		i, ok := i.(map[string]interface{})
		if !ok {
			return false, fmt.Errorf("cache corrompu")
		}
		date, ok := i["date"]
		if !ok {
			return false, fmt.Errorf("cache corrompu")
		}
		message, ok := i["message"]
		if !ok {
			return false, fmt.Errorf("cache corrompu")
		}
		e := Évènement{
			Date:    time.Unix(int64(date.(float64)), 0),
			Message: message.(string),
		}
		if e.Message == "re" {
			e.Type = Retour
		}
		j.Évènements = append(j.Évènements, e)
	}
	if 0 != len(j.Évènements) {
		j.Évènements[0].Type = PremierMessage
	}
	slog.Debug("Évènements chargés depuis le cache.", "clef", clef)
	return true, nil
}

func (j Jour) SauveCache(cache Cacheur) error {
	var l []map[string]interface{}
	for _, e := range j.Évènements {
		l = append(l, map[string]interface{}{
			"date":    e.Date.Unix(),
			"message": e.Message,
		})
	}
	clef := j.Date.Format(YYYYMMDD)
	slog.Debug("Sauvegarde dans le cache.", "clef", clef)
	if nil == l {
		l = make([]map[string]interface{}, 0)
	}
	return cache.Set(clef, l)
}

func (j Jour) Débauche() time.Time {
	return j.Évènements[len(j.Évènements)-1].Date
}

func (j Jour) Embauche() time.Time {
	return j.Évènements[0].Date
}

func (j Jour) IterPrésences() chan time.Duration {
	ch := make(chan time.Duration)
	go func() {
		defer close(ch)
		var début, fin, zero time.Time

		for _, e := range j.Évènements {
			slog.Debug(fmt.Sprintf("> %s: %s", e.Date.Format(HHMMSS), e.Message))

			switch e.Type {
			case PremierMessage:
				début = e.Date
				fin = zero
			case Message:
				fin = e.Date
			case Retour:
				EnvoiPrésence(début, fin, ch)
				début = e.Date
				fin = zero
			}
		}

		EnvoiPrésence(début, fin, ch)
	}()
	return ch
}

func EnvoiPrésence(début, fin time.Time, ch chan time.Duration) {
	if fin.IsZero() {
		return
	}

	présence := fin.Sub(début)
	if présence.Seconds() < 1 {
		return
	}

	slog.Debug("Nouvelle tranche d'activité", "début", début, "fin", fin, "durée", présence)
	ch <- présence
}

type Évènementeur interface {
	Next() bool
	Évènement() Évènement
	Err() error
}

type ListeurJours struct {
	ch   chan Jour
	item Jour
	err  error
}

func NouveauListeurJours(le Évènementeur, cache Cacheur, butoir time.Time) ListeurJours {
	l := ListeurJours{}
	l.ch = l.iterJours(le, cache, butoir)
	return l
}

func (l *ListeurJours) Next() bool {
	jour, ok := <-l.ch
	if !ok {
		return false
	}
	if l.err != nil {
		return false
	}
	l.item = jour
	return true
}

func (l ListeurJours) Jour() Jour {
	return l.item
}

func (l ListeurJours) Err() error {
	return l.err
}

func (l *ListeurJours) iterJours(le Évènementeur, cache Cacheur, butoir time.Time) chan Jour {
	ch := make(chan Jour)
	go func() {
		defer close(ch)
		aujourdhui := time.Now().Truncate(24 * time.Hour)
		for d := range iterDates(butoir) {
			j := Jour{Date: d}
			var err error
			enCache := false
			if j.Date != aujourdhui {
				enCache, err = j.ChargeCache(cache)
				if err != nil {
					l.err = fmt.Errorf("jours: %w", err)
					return
				}
			}

			if !enCache {
				slog.Debug("Récupération des évènements depuis Matrix.", "jour", j.Date.Format(YYYYMMDD))
				e := le.Évènement()
				if e.Date.Truncate(24*time.Hour) == j.Date {
					j.Ajoute(e)
				}
				for le.Next() {
					e := le.Évènement()
					if e.Date.After(j.Date.Add(24 * time.Hour)) {
						continue
					}
					if !j.Contient(e.Date) {
						break
					}
					j.Ajoute(e)
				}
				if err := le.Err(); err != nil {
					l.err = fmt.Errorf("évènements: %w", err)
					return
				}
				if 0 < len(j.Évènements) {
					j.Évènements[0].Type = PremierMessage
				}

				if j.Date != aujourdhui {
					err := j.SauveCache(cache)
					if err != nil {
						l.err = fmt.Errorf("jours: %w", err)
						return
					}
				}
			}
			ch <- j
		}
	}()
	return ch
}

func iterDates(butoir time.Time) chan time.Time {
	ch := make(chan time.Time)
	go func() {
		defer close(ch)
		d := time.Now().Truncate(24 * time.Hour)
		ch <- d
		for d.After(butoir) {
			d = d.Add(-1 * 24 * time.Hour)
			ch <- d
		}
	}()
	return ch
}
