package main

import (
	"flag"
	"fmt"
	"os"
	"runtime"
	"strings"
	"time"

	"log/slog"

	"github.com/matrix-org/gomatrix"
)

var commit string = "inconnu"
var commitCourt string

func init() {
	commitCourt = commit[:7]
}

func pointeuse() error {
	var conf Config

	// Hier au soir, sur le ...
	flag.BoolVar(&conf.Absent, "absent", false, "Considère que l'utilisateur est absent à l'instant.")
	flag.BoolVar(&conf.Bavard, "bavard", false, "Afficher les messages de debug.")
	flag.Parse()
	LireEnvironnement(&conf)
	ConfigurerLogging(conf.Bavard)
	slog.Info("Pointeuse Matrix", "commit", commitCourt, "go", runtime.Version())
	slog.Info(fmt.Sprintf("Connexion à %s en tant que %s.", conf.HomeServer, conf.UserID))

	c, err := gomatrix.NewClient(conf.HomeServer, conf.UserID, conf.AccessToken)
	if err != nil {
		return fmt.Errorf("matrix: %w", err)
	}

	var roomid string
	if strings.HasPrefix(conf.Room, "#") {
		slog.Info(fmt.Sprintf("Recherche du salon %s.", conf.Room))
		roomid, err = RécupérerIdSalon(c, conf.Room)
		if err != nil {
			return fmt.Errorf("room: %w", err)
		}
	} else {
		roomid = conf.Room
	}

	slog.Debug("Récupération des derniers messages.")
	from, _, err := RécupérerDernierÉvènement(c, roomid)
	if err != nil {
		return fmt.Errorf("messages: %w", err)
	}

	butoir := time.Now().Add(-1 * 90 * 24 * time.Hour)
	le := NouveauListeurÉvènements(c, roomid, conf.UserID, from, conf.Absent, butoir)
	cache, err := ChargerCache()
	if err != nil {
		return fmt.Errorf("cache: %w", err)
	}
	lj := NouveauListeurJours(&le, cache, butoir)
	err = Analyser(&lj)
	if err != nil {
		return fmt.Errorf("analyse: %w", err)
	}
	return nil
}

func main() {
	err := pointeuse()
	if err != nil {
		slog.Error("Erreur fatale.", "err", err)
		os.Exit(1)
	}
	os.Exit(0)
}

func ChargerCache() (Cacheur, error) {
	res := os.Getenv("POINTEUSE_CACHE")
	if "0" == res {
		return &FauxCache{}, nil
	}
	return OuvrirCache()
}
