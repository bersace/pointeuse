package main

import (
	"os"
	"path/filepath"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

const HHMMSS = "15:04:05"

type Config struct {
	HomeServer  string `required:"true"`
	UserID      string `required:"true"`
	AccessToken string `required:"true" split_words:"true"`
	Room        string `required:"true"`
	Absent      bool
	Bavard      bool
}

func LireEnvironnement(conf *Config) {
	confdir, _ := os.UserConfigDir()
	rcfile := filepath.Join(confdir, "pointeuserc")
	err := godotenv.Overload(rcfile)
	if err != nil {
		panic(err)
	}
	envconfig.MustProcess("pointeuse", conf)
}
