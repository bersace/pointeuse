package main

import (
	"fmt"
	"time"

	"log/slog"

	"github.com/goodsign/monday"
)

const YYYYMMDD = "2006-01-02"

type Joureur interface {
	Jour() Jour
	Err() error
	Next() bool
}

func Analyser(lj Joureur) error {
	var total_aujourdhui time.Duration

	aujourdhui := time.Now().Truncate(24 * time.Hour)
	total := time.Duration(0)
	jours_travaillés := 0.0
	var debut time.Time

	for lj.Next() {
		j := lj.Jour()
		debut = j.Date

		if 0 == len(j.Évènements) {
			continue
		}

		etp := 0.0
		var total_quotidien time.Duration

		slog.Debug("Nouvelle journée.", "jour", j.Date.Format(YYYYMMDD))
		embauche := j.Embauche()
		if embauche.Hour() < 12 {
			etp += 0.5
		}

		débauche := j.Débauche()
		if débauche.Hour() > 14 {
			etp += 0.5
		}
		jours_travaillés += etp
		slog.Debug(fmt.Sprintf("Embauche à %s, débauche à %s.", embauche.Format(HHMMSS), débauche.Format(HHMMSS)), "jour", j.Date.Format(YYYYMMDD), "total_jour", jours_travaillés)

		for p := range j.IterPrésences() {
			total_quotidien += p
		}
		if aujourdhui == j.Date {
			total_aujourdhui = total_quotidien
		}

		if total_quotidien.Hours() < 1 {
			// Ignorer les jours avec moins d'une heure de présence.
			continue
		}

		if etp > 0 {
			slog.Debug(fmt.Sprintf("Présence du %s: %s.", monday.Format(j.Date, "Monday 2 January", monday.LocaleFrFR), total_quotidien), "etp", etp)
		} else {
			slog.Debug(fmt.Sprintf("Pas assez de données pour le %s.", monday.Format(j.Date, "Monday 2 January", monday.LocaleFrFR)))
		}

		if aujourdhui.YearDay() == j.Date.YearDay() {
			total_aujourdhui = total_quotidien
		}

		total += total_quotidien
	}
	if err := lj.Err(); err != nil {
		return fmt.Errorf("lister jours: %w", err)
	}

	moyenne := time.Duration(float64(total.Nanoseconds()) / jours_travaillés)
	temps_reglementaire, _ := time.ParseDuration("7h30m")
	sup_moy := moyenne - temps_reglementaire
	sup_total_s := fmt.Sprintf("%fs", sup_moy.Seconds()*jours_travaillés)
	sup_total, _ := time.ParseDuration(sup_total_s)
	sup_total = sup_total.Round(time.Minute)
	slog.Info(fmt.Sprintf("Moyenne quotidienne: %s.", moyenne), "début", debut.Format("2006-01-02"), "jours", jours_travaillés, "sup", sup_total)
	if total_aujourdhui.Seconds() > 0 {
		slog.Info(fmt.Sprintf("Présence aujourd'hui: %s.", total_aujourdhui))
	}
	return nil
}
