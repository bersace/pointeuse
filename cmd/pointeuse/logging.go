package main

import (
	"fmt"
	"log/slog"
	"os"

	"github.com/lmittmann/tint"
)

var levelStrings = map[string]string{
	"DEBUG": "\033[2mDEBUG ",
	"INFO":  "\033[1mINFO  ",
	"WARN":  "\033[1;38;5;185mWARN  ",
	"ERROR": "\033[1;31mERROR ",
}

func ConfigurerLogging(bavard bool) {
	level := slog.LevelInfo
	if bavard {
		level = slog.LevelDebug
	}
	slog.SetDefault(slog.New(tint.NewHandler(os.Stderr, BuildTintOptions(level))))
}

func BuildTintOptions(level slog.Level) *tint.Options {
	return &tint.Options{
		Level: level,
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			// Colorize error, level and message from level.
			switch a.Key {
			case slog.LevelKey:
				a.Value = slog.StringValue(levelStrings[a.Value.String()])
			case slog.MessageKey:
				// Reset color after message.
				a.Value = slog.StringValue(fmt.Sprintf("%-32s", a.Value.String()) + "\033[0m")
			default:
				if a.Value.Kind() == slog.KindAny {
					v := a.Value.Any()
					if nil == v && "err" == a.Key {
						// Drop nil error.
						a.Key = ""
						return a
					}
					// Automatic tint.Err()
					err, ok := v.(error)
					if ok {
						a = tint.Err(err)
					}
				}
			}
			return a
		},
		TimeFormat: "15:04:05",
	}
}
