default:

install-master:
	curl --output-dir ~/.local/bin -O https://gitlab.com/api/v4/projects/bersace%2Fpointeuse/packages/generic/go/master/pointeuse
	chmod +x ~/.local/bin/pointeuse
	command pointeuse --help
