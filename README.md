# Pointeuse Matrix

Analyse le temps de présence sur un salon Matrix.

Le temps de présence est déterminé par les dates du premier et du dernier
message du jour (*bonjour* et *au revoir*), en retirant les pauses (entre un
message *pause* et un message de retour *re*). Seul le message *re* indique le
retour d'une pause. N'importe quel message précédent le *re* est considéré comme
le début de la pause. Par exemple *miam*, *afk*, *brb*, etc.


## Installation

``` console
$ curl --output-dir ~/.local/bin -O https://gitlab.com/api/v4/projects/bersace%2Fpointeuse/packages/generic/go/master/pointeuse
$ chmod +x ~/.local/bin/pointeuse
$ pointeuse --help
```


## Configuration

La configuration se fait par variables d'environnement:

- `POINTEUSE_HOMESERVER`
- `POINTEUSE_USERID`
- `POINTEUSE_ACCESS_TOKEN`
- `POINTEUSE_ROOM`, alias du salon.

Pour récupérer votre jeton d'accès dans Element, naviguer dans *Tout les
paramètres*, section *Aide et À propos*. En bas de la page, une section *Avancé*
présent un dérouleur *Jeton d'accès*.

Vous pouvez écrire ces variables d'environnement dans le fichier
`~/.config/pointeuserc` au format .env.


## Fonctionnement

La commande `pointeuse` télécharge l'historique d'un salon, analyse les messages
d'un utilisateur dans un salon Matrix pour compter le temps de présence
quotidien sur le salon. La moyenne du temps de présence quotidien sur les 30
derniers jours glissant est également calculée.

Matrix retourne l'historique dans le sens chronologique inverse. En conséquence,
le temps passée aujourd'hui est analysé en premier. Un rappel du temps de
présence aujourd'hui est affiché à la fin de l'exécution, avec la moyenne.

Par défaut, `pointeuse` considère qu'au moment de l'exécution, l'utilisateur est
toujours présent dans le salon. Pour considérer que le dernier message est le
départ du salon, définir l'option `--absent`.
